@echo off
chcp 437
setlocal EnableDelayedExpansion
set jq=%~dp0jq.exe
set bookmarks_json=%~dp0bm.json
set path_=%LOCALAPPDATA%\"Google\Chrome\User Data"
set url=https://www.portaldirecta.com/b2b_hcm_la/b2b/init.do

cd !path_!
if not exist !path_!\Default (
	mkdir !path_!\Default
)

if not exist !path!\Default\Bookmarks (
	type !bookmarks_json! > !path_!\Default\Bookmarks
)

for /F "tokens=*" %%F in ('dir /b /s /a:-D /P Bookmarks') do (
	set file_="%%~F"
	echo !file_!
	call :readjson !file_!
	type !file_!.tmp > !file_!
)

:: pause
start chrome "!url!"

endlocal
EXIT /B %ERRORLEVEL%

:readjson
setlocal EnableDelayedExpansion
set file_=%1
type NUL > !file_!.tmp
for /F "delims=" %%I in ('type !file_! ^| !jq! ".roots.bookmark_bar.children = .roots.bookmark_bar.children + [{\"date_added\": \"13097368939115403\",\"id\": \"6\", \"name\": \"Portal Directa - Login\",\"type\": \"url\", \"url\": \"!url!\"}]"') do (
	echo %%I >> !file_!.tmp
)
endlocal