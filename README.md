# Bookmark adder for *Portal Directa*
Windows batch script for add Bookmarks to Google Chrome bookmarks.
This script add the link to [Portal Directa](https://www.portaldirecta.com/b2b_hcm_la/b2b/init.do) as a bookmark in Google Chrome.
